
/*
name	oWallpaperFront
x   	64   	X position - COL - Starts from 0, increases in increments of 64
y   	128  	Y position - ROW - Starts from 0, increases in increments of 64 - increment this first
xs  	1    	X scale - Always 4, for this script
ys  	1    	Y scale - Always 4, for this script
type	10   	Frame - Always 0
var1	0    	var1 - Sprite Index: 0-31
var2	0    	var2 - Color: 0-36
*/

async function generateWallpapers()
{
	console.clear();

	const fileTopLines = [
		'1664',
		'1024',
		'2D01000000000000',
		'Wallpapers node.js',
		'1',
		'',
		'0',
	];

	const wallpapersCount = 32; // rows

	const colorBaseCount = 13; // including grey/white
	const colorVariants = 4;

	let wpIndex = 0;
	let colorIndex = 0;
	let colorVariantIndex = 1;
	let currLinesArr = [];
	let outputLines = [];
	let currColorNumber = 0;

	let xyScale = 2;
	let spriteWidth = xyScale * 16

	let colorsStartColumn = 0;

	// Rows
	for( wpIndex = 0; wpIndex < wallpapersCount; wpIndex++ )
	{
		// Reset
		currLinesArr = []
		currColorNumber = 0

		// 13 loops, 1 for each color
		for( colorIndex = 0; colorIndex < colorBaseCount; colorIndex++ )
		{
			if ( colorIndex == 0 )
			{
				// continue; // Uncomment to add white/greys
			}

			// 4 loops, 1 for each variant
			for( colorVariantIndex = 0; colorVariantIndex < colorVariants; colorVariantIndex++ )
			{
				// Ordered
				currColorNumber = ( colorIndex * colorVariants ) + colorVariantIndex;

				// Grouped
				columnNumber = ( colorIndex + colorsStartColumn ) + (colorBaseCount * colorVariantIndex) // eg. colorIndex+1(1) + colorgroups(13) * colorVariantIndex(0)

				currLinesArr.push( ( currColorNumber == 0 ) ? "oWallpaper" : "oWallpaperFront" );
				currLinesArr.push( columnNumber * spriteWidth ); // column
				currLinesArr.push( wpIndex * spriteWidth ); // row
				currLinesArr.push( xyScale );
				currLinesArr.push( xyScale );
				currLinesArr.push( 0 );
				currLinesArr.push( wpIndex ); // sprite
				currLinesArr.push( currColorNumber ); // color

				currColorNumber++; // old, no longer used, kept for clarity
			}
		}

		outputLines.push( currLinesArr.join( '\n' ) );
	}

	const fs = require( 'fs' );
	const { promisify } = require('util');
	const writeFilePromise = promisify( fs.writeFile );

	const finalOutput = fileTopLines.join( '\n' ) + '\n' + outputLines.join( '\n' );

	try
	{
		await writeFilePromise( 'wallpapers-node.nc', finalOutput );
		console.log( 'DONE' );
	}
	catch( err )
	{
		console.log( err );
	}
}

generateWallpapers();
