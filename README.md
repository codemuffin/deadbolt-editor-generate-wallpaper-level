# deadbolt-editor-generate-wallpaper-level

Generates a *.nc file to showcase the new wall colours added to the editor for DECP 4.0.0.

https://deadbolt.codemuffin.com/decp

The output file looks like this. The original wall tiles are in the first column.

![](wall-colors.png)
